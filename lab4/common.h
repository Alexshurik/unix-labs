#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>

#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <pthread.h>

#define SHMname "/SharedData"

#define OK "OK"
#define BAD "BAD"

#define READ_MSG "read"
#define WRITE_MSG "write"
#define STOP_READ_MSG "stop_read"
#define STOP_WRITE_MSG "stop_write"

void die_with_error(char* error_msg);

typedef struct
{
	int val;
} SHMdata;

#endif
