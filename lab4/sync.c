#include "common.h"


#define MAXPENDING 5 /* Maximum outstanding connection requests */

#define WRITE_LOCK -1
#define READ_LOCK 0
#define NO_LOCK 1


int create_tcp_server_socket(int serverPort);
int accept_tcp_connection(int serverSock);
void handle_tcp_client(int clnt_socket, int* rw_lock, int SM_id);
int create_shared_memory();
void set_shared_value(int valuem, int SM_id);

int create_shared_memory()
{
	int fd, err, key, is_rlocked, is_wlocked;
	pthread_rwlockattr_t attr;

	/* Creating shared memory */
	fd = shm_open(SHMname, O_RDWR|O_CREAT|O_EXCL, 0777);
	if (fd < 0) 
	{
		shm_unlink(SHMname);
		fd = shm_open(SHMname, O_RDWR|O_CREAT|O_EXCL, 0777);
		if (fd < 0) die_with_error("Cant't create shmem");
	}

	/* Setting shm size */
	err = ftruncate(fd, sizeof(SHMdata));
	if (err < 0) die_with_error("Can't change shm size");
	
	return fd;
}

void set_shared_value(int value, int SM_id)
{	
	/* Mapping shm content */
	SHMdata *dataPtr = mmap(0, sizeof(SHMdata), PROT_READ|PROT_WRITE, MAP_SHARED, SM_id, 0);
	if ( dataPtr == (void*)-1 ) 
		die_with_error("mmap failed");

	dataPtr->val = value;
	
	/* Munmapping shm content */
	int r = munmap(dataPtr, sizeof(SHMdata));
    	if (r != 0)
		die_with_error("munmap failed");
}

void handle_tcp_client(int clnt_socket, int* rw_lock, int SM_id)
{
     char buffer[256];
     char* serv_ans;

     bzero(buffer,256);
     int n = read(clnt_socket,buffer,255);

     if (n < 0)
	 die_with_error("Error reading from socket");
     /*printf("Prishlo %s\n", buffer);
     printf("Blokirovka bila %d\n", *rw_lock);*/
     
     if(strcmp(buffer, READ_MSG) == 0)
     {
	if (*rw_lock >= 0)
	{
	   *rw_lock = READ_LOCK;
	   serv_ans = OK;
	}
	else
	{
	   serv_ans = BAD;
	}
     }
     else if(strcmp(buffer, WRITE_MSG) == 0)
     {
	if (*rw_lock == NO_LOCK)
	{
	   *rw_lock = WRITE_LOCK;
	   serv_ans = OK;
	}
	else
	{
	   serv_ans = BAD;
	}
     }
     else if(strcmp(buffer, STOP_READ_MSG) == 0)
     {
	if (*rw_lock == READ_LOCK)
	{
	   *rw_lock = NO_LOCK;
	   serv_ans = OK;
	}
	else
	{
	   serv_ans = BAD;
	}
     }
     else if(strcmp(buffer, STOP_WRITE_MSG) == 0)
     {
	if (*rw_lock == WRITE_LOCK)
	{
	   *rw_lock = NO_LOCK;
	   serv_ans = OK;
	}
	else
	{
	   serv_ans = BAD;
	}
     }
     else
     {
	set_shared_value(atoi(buffer), SM_id);
	serv_ans = OK;
     }
     
     /*printf("Blokirovka stala %d\n\n", *rw_lock);*/
     bzero(buffer,256);
     strcpy(buffer, serv_ans);
     n = write(clnt_socket, buffer, strlen(buffer));
    
     if (n < 0) 
	die_with_error("ERROR writing to socket");
}

int create_tcp_server_socket(int serverPort)
{
	int servSock;
	servSock = socket(AF_INET, SOCK_STREAM, 0);
	if (servSock < 0)
		die_with_error("Cant create socket");

	struct sockaddr_in serverAddr;
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port= htons(serverPort);
	serverAddr.sin_addr.s_addr = INADDR_ANY;
	
	if (bind(servSock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
	{
		die_with_error("bind() failed");
	}
	
	if (listen(servSock, MAXPENDING) < 0)
		die_with_error("listen() failed");
	
	return servSock;
}


int accept_tcp_connection(int serverSock)
{
	int clientSock;
	struct sockaddr_in clientAddr;
	int clntLen = sizeof(clientAddr);
	
	if ((clientSock=accept(serverSock,(struct sockaddr *)&clientAddr,&clntLen))<0)
		die_with_error("accept() failed");
	
	return clientSock;
}

int main(int argc, char* argv[])
{
   int servSock;
   int clntSock; 
   int port;
   int read_write_lock = NO_LOCK;
   
   if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
   }

   int SM_id = create_shared_memory();
   port = atoi(argv[1]);
   
   servSock = create_tcp_server_socket(port);

   while (1) 
   { 
	 clntSock = accept_tcp_connection(servSock);
         handle_tcp_client(clntSock, &read_write_lock, SM_id);
         
   }

}
