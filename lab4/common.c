#include "common.h"

void die_with_error(char* errorMsg)
{
	printf("%s\n",errorMsg);
	printf("Error code: %s\n", strerror(errno));
	exit(0);
}
