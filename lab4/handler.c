#include "common.h"


#define MASTER_PORT_NUMBER 8001


void usage();
void set_value(int value);
void get_value();
int get_rw_value(int client_socket);
void test();
void send_value_to_everyone(int value);

void get_value()
{
	int value;
	if(send_opetation_to_master(READ_MSG) == 0)
	{
		printf("Can't read value, because some one is changing it\n");
		return;
	}
	
	/* Opening shared memory */
	int fd = shm_open(SHMname, O_RDWR, 0666);
	if (fd < 0) die_with_error("Cant't open shmem");

	/* Mapping shm content */
	SHMdata *dataPtr = mmap(0, sizeof(SHMdata), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if ( dataPtr == (void*)-1 ) 
		die_with_error("mmap failed");

	value = dataPtr->val;
	
	/* Munmapping shm content */
	int r = munmap(dataPtr, sizeof(SHMdata));
    	if (r != 0)
		die_with_error("munmap");
	send_opetation_to_master(STOP_READ_MSG);

	printf("Value is %d\n", value);
}

void set_value(int value)
{
	if(send_opetation_to_master(WRITE_MSG) == 0)
	{
		printf("Can't set value, because some one is reading or changing it\n");
		return;
	}
	
	send_value_to_everyone(value);
	send_opetation_to_master(STOP_WRITE_MSG);	
	printf("Value was setted\n");
}

void send_value_to_everyone(int value)
{	
	int port;
	int i;
	int machines_connected = 2;
	/* List of all machines connected */
	int ports[2] = {8000, MASTER_PORT_NUMBER};
	
	int client_socket;
	char buffer[256];
	
	int n;
	
	for (i; i < machines_connected; i++)
	{
		port = ports[i];
		bzero(buffer, 256);

		client_socket = create_client_socket(port);
	
		/* Send value to server*/
		sprintf(buffer,"%d", value);
	   	n = write(client_socket, buffer, strlen(buffer));

		if (n < 0) {
	       	 die_with_error("ERROR writing to socket");
		}
	
		/* Read answer from server */
		bzero(buffer,256);
		n = read(client_socket, buffer, 255);
	   
	   	if (n < 0) {
	      	 die_with_error("ERROR reading from socket");
	   	}
	
		close(client_socket);

		if(strcmp(buffer, OK) != 0)
			printf("Something went wrong with machine with port %d\n", port);
	}
}

int send_opetation_to_master(char* operation)
{
	char buffer[256];
	bzero(buffer, 256);

	int client_socket = create_client_socket(MASTER_PORT_NUMBER);
	
	/* Tell server, what operation we wanna do */
	strcpy(buffer, operation);
   	int n = write(client_socket, buffer, strlen(buffer));

        if (n < 0) {
       	 die_with_error("ERROR writing to socket");
        }
	
	/* Read answer from server */
	bzero(buffer,256);
        n = read(client_socket, buffer, 255);
   
   	if (n < 0) {
      	 die_with_error("ERROR reading from socket");
   	}
	
	close(client_socket);

	if(strcmp(buffer, OK) == 0)
		return 1;
	else
		return 0;
}

int create_client_socket(int port)
{
	int clientSock;
	struct sockaddr_in serverAddr;

	clientSock = socket(AF_INET, SOCK_STREAM, 0);

	if (clientSock < 0)
		die_with_error("Cant create client socket");

	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(port);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	if (connect(clientSock, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
		die_with_error("connect() failed");
	
	return clientSock;
}

int main(int argc, char *argv[])
{
    int value;
    char CMD;
    
    while(1)
    {
        printf("\nWaiting for the next command...\n");
        scanf("%c", &CMD);
        getchar();
        printf("\n");
        switch(CMD)
        {
            case 'g': 
              get_value();
              break;
            case 's': 
              printf("\nInput integer value:\n");
              scanf("%d", &value);
       	      getchar();
              printf("\n");
              set_value(value);
              break;
            default:
              usage();
              break;
          }
     }

    return(0);
}

void usage()
{
    fprintf(stderr, "\nUSAGE:\n(g)et\n");
    fprintf(stderr, " (s)et\n");
    fprintf(stderr, " (u)sage\n");
}
