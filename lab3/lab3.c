#include <stdio.h>
#include<fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#define SEM_RESOURCE_MAX 1

/* Initial value of all semaphores */

void createsem(sem_t** sem_id, char* sem_name);
void locksem(sem_t* sid);       
void opensem(sem_t** sem_id, char* sem_name, int* was_opened);                  
void unlocksem(sem_t* sid);
void removesem(char* sem_name, int* success);
void usage(void);

int main(int argc, char *argv[])
{
    sem_t* sem_id;
    char sem_name[250];
    int semaphore_alive;
    
    char CMD;
    int success;
    int was_opened = 0;
    int sem_value;
    
    while(1)
    {
            printf("\nInput semaphore name: \n");
            scanf("%s", sem_name);
            getchar();
            printf("\n");
            semaphore_alive = 1;
            while(semaphore_alive)
            {
                printf("\nWaiting for the next command...\n");
                scanf("%c", &CMD);
                getchar();
                printf("\n");
                switch(CMD)
                {
                    case 'c': 
                      createsem(&sem_id, sem_name);
                      break;
                    case 'l': 
                      opensem(&sem_id, sem_name, &was_opened);
                      if (was_opened)
                      {
                          locksem(sem_id);
                          sem_close(sem_id);
                      }
                      break;
                    case 'u': 
                      opensem(&sem_id, sem_name, &was_opened);
                      if (was_opened)
                      {
                          unlocksem(sem_id);
                          sem_close(sem_id);
                      }
                      break;
                    case 'd':
                      removesem(sem_name, &success);
                      if (success)
                          semaphore_alive = 0;
                      break;
                    case 'v':
                      opensem(&sem_id, sem_name, &was_opened);
                      if (was_opened)
                      {
                          sem_getvalue(sem_id, &sem_value);
                          printf("Sem value is: %d\n", sem_value);
                          sem_close(sem_id);
                      }
                      break;
                    case 'q':
                        return(1);
                        break;
                    default:
                        usage();
                        break;
                  }
            }
    }
    return(0);
}

void createsem(sem_t** sem_id, char* sem_name)
{
    int value;
    *sem_id = sem_open(sem_name, O_CREAT|O_EXCL|O_RDWR, 0666, 1);
    if(*sem_id == SEM_FAILED) {
        printf("Can't create semaphore with name %s!\n", sem_name);
        return;
    }
    printf("Semaphore with name %s was created!\n", sem_name);
    sem_getvalue(*sem_id, &value);
    sem_close(*sem_id);
}


void opensem(sem_t** sem_id, char* sem_name, int* was_opened)
{
    *was_opened = 0;
    *sem_id = sem_open(sem_name,O_RDWR, 0);
    
    if(*sem_id == SEM_FAILED){
        printf("Semaphore with name %s does not exist!\n", sem_name);
        return;
    }
    *was_opened = 1;
}

void unlocksem(sem_t * sid)
{       
    int sem_value;
    sem_getvalue(sid, &sem_value);

    if (sem_value == 1)
    {
        fprintf(stderr, "Semaphore is already unlocked\n");
        return;
    }
    if(sem_post(sid) == -1)
    {
        fprintf(stderr, "Unlock failed\n");
        return;
    }
    else
        printf("Semaphore was unlocked\n");
}

void locksem(sem_t * sid)
{
    int sem_value;
    sem_getvalue(sid, &sem_value);

    if (sem_value == 0)
    {
        fprintf(stderr, "Semaphore is already locked\n");
        return;
    }
    if(sem_trywait(sid) == -1)
    {
        fprintf(stderr, "Lock failed\n");
        return;
    }
    else
        printf("Semaphore was locked\n");
}

void removesem(char * s_name, int* success)
{
    if(sem_unlink(s_name) == -1)
    {
        fprintf(stderr, "Destroy failed\n");
        return;
    }
    printf("Semaphore with name %s was destroyed!\n", s_name);
    *success = 1;
    return;
}

void usage(void)
{
    fprintf(stderr, "\nUSAGE:\n(c)reate\n");
    fprintf(stderr, " (l)ock\n");
    fprintf(stderr, " (u)nlock\n");
    fprintf(stderr, " (d)elete\n");
}

