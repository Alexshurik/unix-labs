Лабораторные работы по ОС Unix

lab 1: Вывести список файлов, начинающихся на заданную букву, находящихся в заданном каталоге

lab 2: Перехватить пользовательский сигнал и вывести его имя

lab 3: Интерактивная работа с семафорами: создание, удаление, открытие, блокировка

lab 4: Разработать программу, синхронизирующую содержимое разных экземпляров разделяемой памяти (на одной или нескольких компьютерах_. При внесении изменения в один из экземпляров, другие должны обновиться. Разработать программу чтения содержимого экземпляра разделяемо памяти (с любого компьютера) с уетом возможного ее изменения в процессе чтения
