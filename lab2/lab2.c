#include<stdio.h>
#include<signal.h>
#include<unistd.h>


static void sig_usr(int);


int main(void)
{	

    printf("My pid is: %d", getpid());

    if (signal(SIGUSR1, sig_usr) == SIG_ERR)
    {
       printf("Can't catch SIGUSR1");
       return -1;
    }

    if (signal(SIGUSR2, sig_usr) == SIG_ERR)
    {
       printf("Can't catch  SIGUSR2");
       return -1;
    }

    while(1)
       pause();
}


static void sig_usr(int signo)
{
    signal(signo, sig_usr);

    if (signo == SIGUSR1)
    {
        printf("Signal SIGUSR1 catched\n");
    }
    else if (signo == SIGUSR2)
    {
        printf("Signal SIGUSR2 catched\n");
    }
}

