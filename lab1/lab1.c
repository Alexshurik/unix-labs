#include <dirent.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        printf("Please, use these format:\nlist_dir dir_path [file_start_symbol]\n");
        return -1;
    }

    char* dir_name = argv[1];
    char* start_symbol;

    if (argc == 3)
    {
        start_symbol = argv[2];
    }

    DIR *d;
    struct dirent *dir;

    d = opendir(argv[1]);

    char* file_name;

    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {
            file_name = dir->d_name;

            if (((start_symbol) && (strncmp(file_name, start_symbol, strlen(start_symbol)) == 0)) || (!start_symbol))
            {
                printf("%s\n", file_name);
            }
        }

        closedir(d);
    }
    else
    {
        printf("Can't read dir \"%s\"\n", argv[1]);
        return -1;
    }

    return 0;
}

